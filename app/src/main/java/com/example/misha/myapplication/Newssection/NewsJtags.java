package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 06.04.2017.
 */

public class NewsJtags {

    public static final String PHOTO = "photo";
    public static final String PHOTOS = "photos";
    public static final String PHOTO_TAGS = "photo_tags";
    public static final String FRIENDS = "friends";
    public static final String NOTE = "note";
    public static final String NOTES = "notes";
    public static final String ATTACHMENT = "attachment";
    public static final String COMMENTS = "comments";
    public static final String LIKES = "likes";
    public static final String AUDIO = "audio";
    public static final String VIDEO = "video";
}
