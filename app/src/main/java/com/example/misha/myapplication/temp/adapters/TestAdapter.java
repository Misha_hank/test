package com.example.misha.myapplication.temp.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.misha.myapplication.temp.models.TestFeedItem;

import java.util.List;

/**
 * Created by djaa on 02.03.17.
 */

public class TestAdapter extends BaseAdapter {

    List<TestFeedItem> feedItemList;

    public TestAdapter(List<TestFeedItem> feedItemList) {
        this.feedItemList = feedItemList;
    }

    @Override
    public int getCount() {
        return feedItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TestFeedItem testFeedItem = feedItemList.get(position);

        return testFeedItem.getView(convertView, parent);
    }
}
