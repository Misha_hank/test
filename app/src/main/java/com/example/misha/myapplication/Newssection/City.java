package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 18.04.2017.
 */

import org.json.JSONException;
import org.json.JSONObject;

public class City {
    public long cid;
    public String name;

    public static City parse(JSONObject o) throws NumberFormatException, JSONException{
        City c = new City();
        c.cid = Long.parseLong(o.getString("cid"));
        c.name = o.getString("name");
        return c;
    }
}