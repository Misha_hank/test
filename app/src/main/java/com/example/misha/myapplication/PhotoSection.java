package com.example.misha.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageButton;

import com.vk.sdk.VKScope;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;

import java.io.IOException;

/**
 * Created by Misha on 24.02.2017.
 */

public class PhotoSection extends Fragment {
    View rootView;

    private ImageButton postphoto;
    public static final int TARGET_GROUP = 60479154;
    public static final int TARGET_ALBUM = 181808365;

    private String[] scope = new String[]{
        VKScope.WALL, VKScope.PHOTOS};

    public static PhotoSection newInstance() {

        Bundle args = new Bundle();

        PhotoSection fragment = new PhotoSection();
        fragment.setArguments(args);
        return fragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.photo, container, false);

//        VKSdk.login(this, scope);
        postphoto = (ImageButton) rootView.findViewById(R.id.post1);
        postphoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Bitmap photo = getPhoto();
                VKRequest request = VKApi.uploadAlbumPhotoRequest(new VKUploadImage(photo, VKImageParameters.pngImage()), TARGET_ALBUM, TARGET_GROUP);
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        recycleBitmap(photo);
                        VKPhotoArray photoArray = (VKPhotoArray) response.parsedModel;
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("https://vk.com/photo-%d_%s", TARGET_GROUP, photoArray.get(0).id)));
                        startActivity(i);

                    }@Override
                    public void onError(VKError error) {
                        super.onError(error);
                    }
                });}});
        return rootView;
    }


    @Nullable
            private Bitmap getPhoto() {
                try {
                    return BitmapFactory.decodeStream(getActivity().getAssets().open("android.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
        }
    }
    private static void recycleBitmap(@Nullable final Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
    }



}
