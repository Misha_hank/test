package com.example.misha.myapplication.temp.interfaces;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.misha.myapplication.temp.models.TestFeedItem;

/**
 * Created by djaa on 02.03.17.
 */

public interface PostItem {

    View getView(View convertView, TestFeedItem feedItem, ViewGroup parent);

}
