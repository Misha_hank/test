package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 18.04.2017.
 */

public class SearchDialogItem {

    public enum SDIType { USER, CHAT, EMAIL }

    public String str_type;
    public SDIType type;
    public String email;
    public User user;
    public Message chat;

}