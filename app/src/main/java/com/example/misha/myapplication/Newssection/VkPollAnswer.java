package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 18.04.2017.
 */
import java.io.Serializable;

public class VkPollAnswer implements Serializable {
    public long id;
    public int votes;
    public String text;
    public int rate;
}
