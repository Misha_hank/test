package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 18.04.2017.
 */

import org.json.JSONException;
import org.json.JSONObject;

public class Country {
    public long cid;
    public String name;

    public static Country parse(JSONObject o) throws NumberFormatException, JSONException{
        Country c = new Country();
        c.cid = Long.parseLong(o.getString("cid"));
        c.name = o.getString("name");
        return c;
    }
}
