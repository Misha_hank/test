package com.example.misha.myapplication;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Misha on 23.01.2017.
 */

public class SendMessage extends Activity{

    ArrayList<String> inList = new ArrayList<>();
    ArrayList<String> outList = new ArrayList<>();
    int id = 0;
    EditText text;
    Button send;
    ListView listView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialogs);
        inList = getIntent().getStringArrayListExtra("in");
        outList = getIntent().getStringArrayListExtra("out");
        id = getIntent().getIntExtra("id", 0);

        Arrays.sort(inList.toArray(), Collections.reverseOrder());
        Arrays.sort(outList.toArray(), Collections.reverseOrder());

        text = (EditText) findViewById(R.id.text);
        listView = (ListView) findViewById(R.id.list_msg);


        Log.e("OnItemClickListener","inlist size :: " + inList.size());
        Log.e("OnItemClickListener","outlist size :: " + outList.size());
        // Думаю здесь нужен отдельный адаптер для сообщений
        listView.setAdapter(new CustomAdapter(this, inList,outList));
        send = (Button) findViewById(R.id.send1);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {VKRequest vkRequest = new VKRequest("messages.send", VKParameters.from(VKApiConst.USER_ID, id,
                    VKApiConst.MESSAGE, text.getText().toString()));



                vkRequest.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        Log.e("OnMessageSent", "Message sent");
                        super.onComplete(response);
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        Log.e("attemptFailed","attemptFailed :: " + request.toString());
                        super.attemptFailed(request, attemptNumber, totalAttempts);
                    }

                    @Override
                    public void onError(VKError error) {
                        Log.e("attemptFailed","error :: " + error.toString());
                        super.onError(error);
                    }
                });


            }
        });
    }
}
