package com.example.misha.myapplication.Newssection;



import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.misha.myapplication.R;

import java.security.acl.Group;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NewsSection {


    public ArrayList<NewsItems> items=new ArrayList<NewsItems>();
    public ArrayList<User> profiles;
    public ArrayList<Group> groups;
    public int new_from;
    public int new_offset;
    public JSONObject root_raw_response;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.news_view_fragment, container, false);



        return rootView;}

    public static NewsSection parse(JSONObject root, boolean is_comments) throws JSONException {
        JSONObject response1 = root.getJSONObject("response");
        JSONArray jitems = response1.optJSONArray("items");
        JSONArray jprofiles = response1.optJSONArray("profiles");
        JSONArray jgroups = response1.optJSONArray("groups");
        NewsSection newsfeed = new NewsSection();

        newsfeed.root_raw_response = root;

        newsfeed.new_from = response1.optInt("new_from");
        newsfeed.new_offset = response1.optInt("new_offset");

        if (jitems != null) {
            newsfeed.items = new ArrayList<NewsItems>();
            for(int i = 0; i < jitems.length(); i++) {
                JSONObject jitem = (JSONObject)jitems.get(i);
                NewsItems newsitem = NewsItems.parse(jitem, is_comments);
                newsfeed.items.add(newsitem);
            }
        }

        if (jprofiles != null) {
            newsfeed.profiles = new ArrayList<User>();
            for(int i = 0; i < jprofiles.length(); i++) {
                JSONObject jprofile = (JSONObject)jprofiles.get(i);
                User m = User.parseFromNews(jprofile);
                newsfeed.profiles.add(m);
            }
        }
        if (jgroups != null)
            newsfeed.groups = Group.parseGroups(jgroups);
        return newsfeed;
    }
}