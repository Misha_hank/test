package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 18.04.2017.
 */

import org.json.JSONException;
import org.json.JSONObject;

public class FriendList {

    public int lid;
    public String name;

    public static FriendList parse(JSONObject o) throws JSONException {
        FriendList fl = new FriendList();
        fl.lid = o.getInt("lid");
        fl.name = o.getString("name");
        return fl;
    }
}
