package com.example.misha.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.misha.myapplication.Newssection.NewsSection;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.methods.VKApiWall;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiGetDialogResponse;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.photo.VKImageParameters;
import com.vk.sdk.api.photo.VKUploadImage;


import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private String[] scope = new String[]{VKScope.MESSAGES, VKScope.FRIENDS, VKScope.WALL, VKScope.PHOTOS};
    private ListView listView;
    private Button showMessage;
    private VKList list;
    private Button postphoto;
    public static final int TARGET_GROUP = 60479154;
    public static final int TARGET_ALBUM = 181808365;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        setContentView(R.layout.drawer_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView = (ListView) findViewById(R.id.list_view);

        VKSdk.login(this, scope);

        postphoto = (Button) findViewById(R.id.share);
        showMessage = (Button) findViewById(R.id.showMessage);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        showMessage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                final VKRequest request = VKApi.messages().getDialogs(VKParameters.from(VKApiConst.COUNT, 10));
                VKRequest re = new VKRequest("newsfeed.get", VKParameters.from(VKApiConst.COUNT, 10));
                re.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        Log.d("VK response", "onComplete :: " + response.responseString);
                        super.onComplete(response);
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        Log.d("VK response", "attemptFailed :: " + request.toString());

                        super.attemptFailed(request, attemptNumber, totalAttempts);
                    }

                    @Override
                    public void onError(VKError error) {
                        Log.d("VK response", "onError :: " + error.toString());

                        super.onError(error);
                    }
                });



                request.executeWithListener(new VKRequest.VKRequestListener(){
                    @Override
                    public void onComplete(VKResponse response){
                        super.onComplete(response);

                        VKApiGetDialogResponse getDialogResponse = (VKApiGetDialogResponse) response.parsedModel;

                        final VKList<VKApiDialog> list = getDialogResponse.items;

                        ArrayList<String> messages = new ArrayList<>();
                        ArrayList<String> users = new ArrayList<>();

                        for (VKApiDialog msg:list) {
                            users.add(String.valueOf(MainActivity.this.list.getById(msg.message.user_id)));
                            messages.add(msg.message.body);
                        }

                        listView.setAdapter(new CustomAdapter(MainActivity.this, users, messages, list));
                    }
                });
            }
        });
                postphoto.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                       final Bitmap photo = getPhoto();
                        VKRequest request = VKApi.uploadAlbumPhotoRequest(new VKUploadImage(photo, VKImageParameters.pngImage()), TARGET_ALBUM, TARGET_GROUP);
                        request.executeWithListener(new VKRequest.VKRequestListener() {
                            @Override
                            public void onComplete(VKResponse response) {
                                recycleBitmap(photo);
                                VKPhotoArray photoArray = (VKPhotoArray) response.parsedModel;
                                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("https://vk.com/photo-%d_%s", TARGET_GROUP, photoArray.get(0).id)));
                                startActivity(i);

                            }@Override
                            public void onError(VKError error) {
                                super.onError(error);
                            }
                        });



                    }
                });


        // Лучше всего вешать листенер на ListView, чем делать это в адаптере,
        // вообще старайся в адаптере не использовать листенеры, или хотябы делать калбэки
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Log.e("OnItemClickListener","onitemclicklistener");
                final int user_id = ((VKApiDialog) parent.getAdapter().getItem(position)).message.user_id;
//                final int id = list.get(position).message.user_id;

                VKRequest request = new VKRequest("messages.getHistory", VKParameters.from(VKApiConst.USER_ID, user_id));
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);

                        Log.e("OnItemClickListener","on Complete :: " + response.json.toString());

                        ArrayList<String> inList = new ArrayList<>();
                        ArrayList<String> outList = new ArrayList<>();

                        try {
                            JSONArray array = response.json.getJSONObject("response").getJSONArray("items");
                            VKApiMessage[] msg = new VKApiMessage[array.length()];
                            for (int i=0; i < array.length();i++ )
                            {
                                VKApiMessage mes = new VKApiMessage(array.getJSONObject(i));
                                msg[i]=mes;
                            }
                            for(VKApiMessage mess: msg){
                                if(mess.out){
                                    outList.add(mess.body);
                                }else{inList.add(mess.body);}
                            }

                            Log.e("OnItemClickListener","inlist size :: " + inList.size());
                            Log.e("OnItemClickListener","outlist size :: " + outList.size());

                            MainActivity.this.startActivity(new Intent(MainActivity.this, SendMessage.class).putExtra("id", user_id)
                                    .putExtra("in", inList).putExtra("out", outList));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }







                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        Log.e("OnItemClickListener", " onAttempt fail");
                        super.attemptFailed(request, attemptNumber, totalAttempts);
                    }

                    @Override
                    public void onError(VKError error) {
                        Log.e("OnItemClickListener", " onError, error:: " + error.toString());

                        super.onError(error);
                    }
                });

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {


                VKRequest request = VKApi.friends().get(VKParameters.from(VKApiConst.FIELDS, "first_name, last_name"));
                request.executeWithListener(new VKRequest.VKRequestListener(){
                    @Override
                    public void onComplete(VKResponse response){
                        super.onComplete(response);
                        list = (VKList) response.parsedModel;
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(MainActivity.this,
                                android.R.layout.simple_expandable_list_item_1, list);

                        listView.setAdapter(arrayAdapter);
                    }
                });
            }
            @Override
            public void onError(VKError error) {

            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Nullable
    private Bitmap getPhoto() {
        try {
            return BitmapFactory.decodeStream(getAssets().open("android.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    private static void recycleBitmap(@Nullable final Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
    }
    private File getFile() {
        try {
            InputStream inputStream = getAssets().open("android.jpg");
            File file = new File(getCacheDir(), "android.jpg");
            OutputStream output = new FileOutputStream(file);
            byte[] buffer = new byte[4 * 1024]; // or other buffer size
            int read;

            while ((read = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, read);
            }
            output.flush();
            output.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        Fragment fragment = null;
        Class fragmentClass = null;

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.news_feed) {
            fragmentClass = NewsSection.class;

        } else if (id == R.id.msgs) {
            fragmentClass = MessageSection.class;
        } else if (id == R.id.foto) {
            fragmentClass = PhotoSection.class;

        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Вставляем фрагмент, заменяя текущий фрагмент
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.activity_main, fragment).commit();

        // Выделяем выбранный пункт меню в шторке
        item.setChecked(true);
        // Выводим выбранный пункт в заголовке
        setTitle(item.getTitle());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

