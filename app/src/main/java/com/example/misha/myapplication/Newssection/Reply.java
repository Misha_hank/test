package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 18.04.2017.
 */

import java.io.Serializable;
import org.json.JSONException;
import org.json.JSONObject;

public class Reply implements Serializable{

    private static final long serialVersionUID = 1L;
    public long id;
    public long date;
    public String text;

    public static Reply parse(JSONObject o) throws JSONException {
        Reply r = new Reply();
        r.id = o.getLong("id");
        r.date = o.getLong("date");
        r.text = o.getString("text");
        return r;
    }

}
