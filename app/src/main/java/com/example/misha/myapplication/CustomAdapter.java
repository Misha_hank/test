package com.example.misha.myapplication;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiDialog;
import com.vk.sdk.api.model.VKApiMessage;
import com.vk.sdk.api.model.VKList;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import static android.R.id.message;

/**
 * Created by Misha on 20.01.2017.
 */
public class CustomAdapter extends BaseAdapter {
    private ArrayList<String> users, messeges;
    private Context context;
    private VKList<VKApiDialog> list;

    public CustomAdapter(Context context, ArrayList<String> users, ArrayList<String> messeges, VKList<VKApiDialog> list) {
        this.users = users;
        this.messeges = messeges;
        this.context = context;
        this.list = list;
    }


    public CustomAdapter(Context context, ArrayList<String> users, ArrayList<String> messeges) {
        this.users = users;
        this.messeges = messeges;
        this.context = context;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public VKApiDialog getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        SetData setData = new SetData();
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_view, null);
        setData.user_name = (TextView) view.findViewById(R.id.user_name);
        setData.msg = (TextView) view.findViewById(R.id.msg);
        try {
            setData.user_name.setText(users.get(position));
            setData.msg.setText(messeges.get(position));
        } catch (IndexOutOfBoundsException ignored){
            // TODO: Prevent  this exception it.

        }

//        if(list==null)


        return view;
    }
    public class SetData{
        TextView user_name, msg;
    }
}
