package com.example.misha.myapplication.Newssection;

/**
 * Created by Misha on 06.04.2017.
 */

public class NewsTypes extends NewsItems {
    public static final String POST = "post";
    public static final String PHOTO = "photo";
    public static final String PHOTO_TAG = "photo_tag";
    public static final String FRIEND = "friend";
    public static final String NOTE = "note";
    public static final String AUDIO = "audio";
    public static final String VIDEO = "video";
}