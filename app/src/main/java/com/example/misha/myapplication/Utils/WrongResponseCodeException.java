package com.example.misha.myapplication.Utils;

/**
 * Created by Misha on 18.04.2017.
 */
import java.io.IOException;

public class WrongResponseCodeException extends IOException {
    private static final long serialVersionUID = 1L;

    public WrongResponseCodeException(String message) {
        super(message);
    }

}
